import React, { MouseEvent } from 'react';
import clsx from 'clsx';
import Button from '../../common/Button';
import H3 from '../../common/H3';

export interface SectionProps {
  children?: React.ReactNode;
  className?: string;
  isActive?: boolean;
  isFirst?: boolean;
  isLast?: boolean;
  onNextClick(event: MouseEvent<HTMLButtonElement>): void;
  onPrevClick(event: MouseEvent<HTMLButtonElement>): void;
  title: string;
}

export default function Section({
  children,
  className,
  isActive,
  isFirst,
  isLast,
  onNextClick,
  onPrevClick,
  title,
}: SectionProps): JSX.Element {
  const classes = clsx(isActive ? 'flex' : 'hidden', 'flex-wrap w-full', className);

  return (
    <div className={classes}>
      <H3 className="w-full mb-6">{title}</H3>
      <br className="w-full border-t border-gray-500" />
      {children}
      <div className="w-full grid grid-cols-2">
        <div className="justify-self-start">
          {!isFirst && (
            <Button type="button" onClick={onPrevClick}>
              Prev
            </Button>
          )}
        </div>
        <div className="justify-self-end">
          {
            <Button type="button" onClick={onNextClick}>
              {isLast ? 'Submit' : 'Next'}
            </Button>
          }
        </div>
      </div>
    </div>
  );
}
