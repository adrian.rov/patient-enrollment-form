import React, { forwardRef, ForwardedRef } from 'react';
import clsx from 'clsx';
import Option, { OptionProps } from './Option';

export interface SelectProps extends React.HTMLProps<HTMLSelectElement> {
  className?: string;
  id: string;
  label: string;
  defaultValueText?: string;
  labelClassName?: string;
  selectClassName?: string;
  options: OptionProps[];
  useDefaultValue?: boolean;
}

const Select = (
  {
    className,
    id,
    defaultValueText,
    selectClassName,
    label,
    labelClassName,
    options,
    useDefaultValue,
    ...props
  }: SelectProps,
  ref: ForwardedRef<HTMLSelectElement>,
): JSX.Element => {
  const classes = clsx('mb-4', className);
  const selectClasses = clsx(
    'w-full mt-3 text-sm form-select md:text-base placeholder-gray rounded-md border-gray-300',
    selectClassName,
  );
  const labelClasses = clsx('font-semibold text-primary', labelClassName);

  return (
    <div className={classes}>
      <label className={labelClasses} htmlFor={id}>
        {label}
      </label>
      <select
        ref={ref}
        className={selectClasses}
        defaultValue={props.multiple ? [''] : ''}
        id={id}
        {...props}
      >
        <Option
          disabled={!useDefaultValue}
          hidden={!useDefaultValue}
          value=""
          text={defaultValueText || label}
        ></Option>
        {options.map((optionProps: OptionProps) => (
          <Option key={optionProps.value} {...optionProps} />
        ))}
      </select>
    </div>
  );
};

export default forwardRef(Select);
