import React from 'react';
import clsx from 'clsx';

export default function Container({
  children,
  className,
}: {
  children?: React.ReactNode;
  className?: string;
}): JSX.Element {
  const classes = clsx(
    'bg-gray-100 flex flex-col items-center justify-center min-h-screen h-full p-x-2',
    className,
  );

  return <div className={classes}>{children}</div>;
}
