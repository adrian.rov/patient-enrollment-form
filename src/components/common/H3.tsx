import React from 'react';
import clsx from 'clsx';

export default function H3({
  children,
  className,
}: {
  children?: React.ReactNode;
  className?: string;
}): JSX.Element {
  const classes = clsx(
    'font-serif font-semibold text-primary text-xl lg:text-h3 leading-extra-tight',
    className,
  );

  return <h3 className={classes}>{children}</h3>;
}
