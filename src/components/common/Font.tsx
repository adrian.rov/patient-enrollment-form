import React, { useState } from 'react';

export default function Font({ href }: { href: string }): JSX.Element {
  const [media, setMedia] = useState('all');
  const onLoad = () => {
    setMedia('all');
  };
  return (
    <>
      <link rel="preload" as="style" href={href} />
      <link rel="stylesheet" href={href} media={media} onLoad={onLoad} />
      <noscript>
        <link rel="stylesheet" href={href} />
      </noscript>
    </>
  );
}
